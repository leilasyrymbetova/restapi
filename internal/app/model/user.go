package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
	"golang.org/x/crypto/bcrypt"
	"log"
)

type User struct {
	ID           int    `json:"ID"`
	Email        string `json:"Email"`
	OpenPassword string `json:"OpenPassword,omitempty"`
	Password     string `json:"-"`
}

func (u *User) Validate() error {
	return validation.ValidateStruct(u,
		validation.Field(&u.Email, validation.Required, is.Email),
		validation.Field(&u.OpenPassword, validation.By(requiredIf(u.Password == "")), validation.Length(6, 100)),
	)
}

func (u *User) BeforeCreate() error {
	if len(u.OpenPassword) > 0 {
		enc, err := encryptString(u.OpenPassword)
		if err != nil {
			log.Fatal(err)
		}
		u.Password = enc
	}
	return nil
}

func encryptString(s string) (string, error) {
	b, err := bcrypt.GenerateFromPassword([]byte(s), bcrypt.MinCost)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func (u *User) Sanitize() {
	u.OpenPassword = ""
}

func (u *User) ComparePassword(OpenPassword string) bool {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(OpenPassword)) == nil
}
